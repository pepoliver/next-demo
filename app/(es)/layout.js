import Header from '@/app/components/Header/Header';
import '../globals.css';

export const metadata = {
  title: 'DEMO | Inicio',
};

export default function RootLayout({ children }) {
  return (
    <html lang="es">
      <body>
        <Header />
        {children}
      </body>
    </html>
  );
}
