import AboutUs from '@/app/components/AboutUs/AboutUs';

export const metadata = {
  title: 'DEMO | Sobre nosotros',
};

export default function AboutUsPage() {
  return <AboutUs lang="es" />;
}
