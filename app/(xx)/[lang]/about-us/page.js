import AboutUs from '@/app/components/AboutUs/AboutUs';
import getText from '@/app/lib/getText';

export async function generateMetadata({ params }) {
  let { lang } = params;

  const title = await getText({ lang, textKey: 'about-us.title' });
  return {
    title: `DEMO | ${title}`,
  };
}

export default function AboutUsPage({ params }) {
  return <AboutUs lang={params.lang} />;
}
