import ExternalPlugin from '@/app/plugins/external/ExternalPlugin';

export default async function CustomPage({ params }) {
  const { lang, pageId } = params;

  return (
    <div className="p-10">
      <h1>My custom page</h1>
      <ExternalPlugin
        type="custom-page"
        componentProps={{
          pageId,
          lang,
        }}
      />
    </div>
  );
}
