import Header from '@/app/components/Header/Header';
import '../../globals.css';

export default function RootLayout({ children, params }) {
  return (
    <html lang={params.lang}>
      <body>
        <Header />
        {children}
      </body>
    </html>
  );
}
