import Home from '@/app/components/Home/Home';
import getText from '@/app/lib/getText';

export async function generateMetadata({ params }) {
  const { lang } = params;

  const title = await getText({ lang, textKey: 'home.title' });
  return {
    title: `DEMO | ${title}`,
  };
}

export default function HomePage({ params }) {
  return <Home lang={params.lang} />;
}
