import { NextResponse } from 'next/server';

export async function GET() {
  return NextResponse.json([
    {
      source: '/ca/sobre-nosaltres',
      destination: '/ca/about-us',
      type: 'rewrite',
      searchParams: {},
    },
    {
      source: '/sobre-nosotros-personalizado',
      destination: '/sobre-nosotros',
      type: 'rewrite',
      searchParams: {},
    },
    {
      source: '/ca/sobre-nosaltres-personalitzat',
      destination: '/ca/about-us',
      type: 'rewrite',
      searchParams: {},
    },
    {
      source: '/mi-pagina-especial',
      destination: '/es/custom-page/23',
      type: 'rewrite',
      searchParams: {},
    },
    {
      source: '/sobre-nosotros-redireccionado-dinamico',
      destination: '/sobre-nosotros',
      type: 'redirect',
      statusCode: 307,
    },
  ]);
}
