import { NextResponse } from 'next/server';

export async function GET() {
  return NextResponse.json({
    default: {
      es: {
        home: {
          title: 'Inicio',
          link1: 'Sobre nosotros',
        },
        'about-us': {
          title: 'Título por defecto sobre nosotros',
        },
      },
      ca: {
        home: {
          title: 'Inici',
          link1: 'Sobre nosaltres',
        },
        'about-us': {
          title: 'Títol per defecte sobre nosaltres',
        },
      },
    },
    'client-1234': {
      es: {
        'about-us': {
          title: 'Título para el cliente client-1234',
        },
      },
      ca: {
        'about-us': {
          title: 'Títol per al client client-1234',
        },
      },
    },
  });
}
