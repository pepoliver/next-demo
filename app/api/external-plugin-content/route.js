export async function GET(request) {
  const { searchParams } = new URL(request.url);
  const type = searchParams.get('type');
  const componentPropsString = searchParams.get('component-props');

  return new Response(
    `<div class="p-10 border">
            <p>This is the content for the plugin type "${type}" and this are the props received ${componentPropsString}.</p>
            <button class="pt-5 underline" onClick="onClickFunction()">Click here to say hi to the parent component</button>
            <div id="demo"></div>
            
            <script>
            
            function onClickFunction() {
              window.postMessage({
                message: 'HI from the plugin component!',
                source: 'plugin',
              });
              
            }

            function receiveMessage({ data }) {
              if (data.source === 'parent') {
                document.getElementById('demo').innerHTML = data.message;
              }
            }

            window.addEventListener('message', receiveMessage);
            </script>
        </div>`,
    {
      headers: {
        'content-type': 'text/html',
      },
    }
  );
}
