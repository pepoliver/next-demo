import setSession from '@/app/lib/session/setSession';
import { NextResponse } from 'next/server';

export async function POST(request) {
  const { username, password } = await request.json();
  console.log(username, password);

  setSession({
    name: 'John Doe',
    clientId: 'client-1234',
  });

  return NextResponse.json({ success: true });
}
