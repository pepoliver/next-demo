import setSession from '@/app/lib/session/setSession';
import { NextResponse } from 'next/server';

export async function POST() {
  setSession(null);

  return NextResponse.json({ success: true });
}
