import React from 'react';
import Text from '@/app/components/Text/Text';

export default function AboutUs({ lang = 'es' }) {
  return (
    <main className="flex min-h-screen flex-col items-center p-24">
      <h1>
        <Text lang={lang} textKey={'about-us.title'} />
      </h1>
    </main>
  );
}
