import Link from 'next/link';

export default function DefaultHeader({ loginLogoutButton }) {
  return (
    <div className="p-3 flex justify-between items-center w-100 bg-gray-700">
      <span>Demo APP</span>
      <div className="flex gap-3 items-center">
        {loginLogoutButton}

        <Link href={'/'} className="underline">
          Castellano
        </Link>

        <Link href={'/ca'} className="underline">
          Català
        </Link>
      </div>
    </div>
  );
}
