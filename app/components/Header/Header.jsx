import LoginLogoutButton from './LoginLogoutButton/LoginLogoutButton';
import DefaultHeader from './DefaultHeader/DefaultHeader';
import InHousePlugin from '@/app/plugins/inHouse/InHousePlugin';

export default async function Header() {
  return (
    <InHousePlugin
      type="header"
      DefaultComponent={DefaultHeader}
      componentProps={{
        loginLogoutButton: <LoginLogoutButton />,
      }}
    />
  );
}
