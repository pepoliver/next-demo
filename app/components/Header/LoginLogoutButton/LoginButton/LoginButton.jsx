'use client';

import useLoginButton from './useLoginButton';

export default function LoginButton() {
  const { state, actions } = useLoginButton();
  return (
    <button disabled={state.loading} onClick={actions.login}>
      Login
    </button>
  );
}
