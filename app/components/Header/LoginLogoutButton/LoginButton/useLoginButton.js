import { useState } from 'react';

export default function useLoginButton() {
  const [state, setState] = useState({
    loading: false,
  });

  const actions = {
    login: async () => {
      setState({ loading: true });
      await fetch('/api/login', {
        method: 'POST',
        body: JSON.stringify({ username: 'test', password: 'test' }),
      });
      setState({ loading: false });
      document.location.reload();
    },
  };

  return { state, actions };
}
