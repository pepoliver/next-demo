import getSession from '@/app/lib/session/getSession';
import LoginButton from './LoginButton/LoginButton';
import LogoutButton from './LogoutButton/LogoutButton';

export default function LoginLogoutButton() {
  const session = getSession();

  return session ? (
    <div className="flex gap-5 items-center">
      <span className="font-bold">{session.name}</span>
      <LogoutButton />
    </div>
  ) : (
    <LoginButton />
  );
}
