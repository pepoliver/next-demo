'use client';

import useLogoutButton from './useLogoutButton';

export default function LogoutButton() {
  const { state, actions } = useLogoutButton();
  return (
    <button disabled={state.loading} onClick={actions.logout}>
      Logout
    </button>
  );
}
