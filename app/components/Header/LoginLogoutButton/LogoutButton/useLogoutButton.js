import { useState } from 'react';

export default function useLogoutButton() {
  const [state, setState] = useState({
    loading: false,
  });

  const actions = {
    logout: async () => {
      setState({ loading: true });
      await fetch('/api/logout', {
        method: 'POST',
      });
      setState({ loading: false });
      document.location.reload();
    },
  };

  return { state, actions };
}
