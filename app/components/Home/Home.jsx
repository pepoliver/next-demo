import Link from 'next/link';
import Text from '@/app/components/Text/Text';

export default function Home({ lang }) {
  return (
    <main className="flex min-h-screen flex-col items-center p-24">
      <h1 className="pb-10">
        <Text lang={lang} textKey="home.title" />
      </h1>
      <ul>
        <li>
          <Link href={getButtonUrl(lang)} className="underline">
            <Text lang={lang} textKey="home.link1" />
          </Link>
        </li>
      </ul>
    </main>
  );
}

function getButtonUrl(lang) {
  return {
    es: '/sobre-nosotros',
    ca: 'ca/sobre-nosaltres',
  }[lang];
}
