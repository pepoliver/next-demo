import getText from '@/app/lib/getText';

export default async function Text({ lang, textKey }) {
  const text = await getText({ lang, textKey });
  return text || null;
}
