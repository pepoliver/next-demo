import { headers } from 'next/headers';

export default function getClientId() {
  return headers().get('x-client-id');
}
