import getClientId from './client/getClient';

let dictionary = null;

export default async function getText({ lang, textKey }) {
  const clientId = getClientId() || 'default';
  if (!dictionary) {
    const response = await fetch(`http://localhost:3000/api/dictionary`);
    dictionary = await response.json();
  }
  const value = getValueFromPath(dictionary[clientId][lang], textKey);
  return value || getValueFromPath(dictionary.default[lang], textKey);
}

function getValueFromPath(obj, path) {
  const keys = path.split('.');
  let result = obj;

  for (const key of keys) {
    if (result?.[key] === undefined) {
      return undefined;
    }
    result = result?.[key];
  }

  return result;
}
