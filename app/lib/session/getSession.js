import { cookies } from 'next/headers';

export default function getSession() {
  const session = cookies().get('session')?.value;
  return session ? JSON.parse(session) : null;
}
