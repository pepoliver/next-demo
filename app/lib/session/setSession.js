import { cookies } from 'next/headers';

export default function setSession(sessionData) {
  if (sessionData) {
    cookies().set('session', JSON.stringify(sessionData), {
      httpOnly: true,
      secure: process.env.NODE_ENV === 'production',
      maxAge: 60 * 60 * 24 * 7, // One week
      path: '/',
    });
  } else {
    cookies().delete('session');
  }
}
