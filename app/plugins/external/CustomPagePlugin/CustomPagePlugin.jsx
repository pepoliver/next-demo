import CustomPagePluginBridge from './CustomPagePluginBridge';

export default function CustomPagePlugin({ html }) {
  return (
    <>
      <CustomPagePluginBridge />
      <div dangerouslySetInnerHTML={{ __html: html }} />
    </>
  );
}
