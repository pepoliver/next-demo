'use client';

import { useEffect, useState } from 'react';

export default function CustomPagePluginBridge() {
  const [receivedMessage, setReceivedMessage] = useState('');

  useEffect(() => {
    function receiveMessage({ data }) {
      if (data.source === 'plugin') {
        setReceivedMessage(data.message);
      }
    }

    window.addEventListener('message', receiveMessage);
    return () => {
      window.removeEventListener('message', receiveMessage);
    };
  }, []);

  const sendMessage = (message) => {
    window.postMessage({
      message,
      source: 'parent',
    });
  };

  return (
    <div className="flex gap-3">
      <button
        className="pb-5 underline"
        onClick={() => sendMessage('Hi from the parent!')}
      >
        Send &quot;Hi!&quot; to plugin
      </button>
      <div>{receivedMessage}</div>
    </div>
  );
}
