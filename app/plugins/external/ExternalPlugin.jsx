import CustomPagePlugin from './CustomPagePlugin/CustomPagePlugin';

export default async function ExternalPlugin({ type, componentProps }) {
  const searchParams = new URLSearchParams();
  searchParams.set('type', type);
  searchParams.set('component-props', JSON.stringify(componentProps));
  searchParams.set('d', Date.now());

  const response = await fetch(
    `http://localhost:3000/api/external-plugin-content?${searchParams.toString()}`
  );
  const html = await response.text();

  const Component = {
    'custom-page': CustomPagePlugin,
  }[type];

  return <Component {...componentProps} html={html} />;
}
