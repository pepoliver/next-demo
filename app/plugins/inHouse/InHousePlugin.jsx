import dynamic from 'next/dynamic';
import getClientId from '../../lib/client/getClient';

const clientComponents = {
  'client-1234': {
    header: dynamic(() => import('./client-1234/Header')),
  },
};

export default async function InHousePlugin({
  type,
  DefaultComponent,
  componentProps,
}) {
  const clientId = getClientId();
  const Component = clientComponents[clientId]?.[type] || DefaultComponent;
  return <Component {...componentProps} />;
}
