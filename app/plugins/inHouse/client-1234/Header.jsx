import Link from 'next/link';

export default function Header({ loginLogoutButton }) {
  return (
    <div className="p-3 flex justify-between items-center w-100 bg-blue-700">
      <Link href={'/'}>Client 1234 APP</Link>

      <Link href={'/mi-pagina-especial'} className="underline">
        Mi pagina especial
      </Link>
      <div className="flex gap-3 items-center">
        {loginLogoutButton}

        <Link href={'/'} className="underline">
          ES
        </Link>

        <Link href={'/ca'} className="underline">
          CA
        </Link>
      </div>
    </div>
  );
}
