import { NextResponse } from 'next/server';

export async function middleware(request) {
  const pathname = request.nextUrl.pathname;

  let apiResponse = await fetch(`http://localhost:3000/api/custom-routes`);

  const customRoutes = await apiResponse.json();
  const customRoute = customRoutes.find((route) => route.source === pathname);

  let response;
  if (customRoute) {
    const url = new URL(customRoute.destination, request.url);

    if (customRoute.searchParams) {
      for (const [key, value] of Object.entries(customRoute.searchParams)) {
        url.searchParams.set(key, value);
      }
    }
    response = NextResponse[customRoute.type](url, customRoute.statusCode);
  } else {
    response = NextResponse.next();
  }

  // You can define the clientId depending on the domain or anything you need to load the custom plugins
  if (request.url.startsWith('http://localhost:3000/')) {
    response.headers.set('x-client-id', 'client-1234');
  }
  return response;
}
