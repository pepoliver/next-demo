const nextConfig = {
  async redirects() {
    return [
      {
        source: '/sobre-nosotros-redireccionado-fijo',
        destination: '/sobre-nosotros',
        permanent: true,
      },
    ];
  },
};

export default nextConfig;
